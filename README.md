<div align="center">

<p align="center">
  <a href="" rel="noopener">
    <img width="30%" src="./ansible-wordmark.png" alt="Ansible Wordmark"></a>
    <br>
    <img width="70%"src="./retropie-logo.png" alt="RetroPie logo">
  </a>
</p>

  [![Status](https://img.shields.io/badge/status-Active-success.svg)]() 
  [![Ansible Galaxy](https://img.shields.io/ansible/role/TODO)]() 
  [![Ansible Version](https://img.shields.io/badge/ansible-TODO-informational)]() 
  [![Ansible Version](https://img.shields.io/ansible/quality/TODO)]() 
  [![License](https://img.shields.io/badge/license-GPLv3-blue.svg)](/LICENSE)

</div>

---

# Ansible App - RetroPie

Takes a Debian system and sets up Retropie for it.

- Installs dependencies
- Sets up users and groups
- Clones the repo
- Installs using unattended script

## 🦺 Requirements

*None*

## 🗃️ Role Variables

*None*

## 📦 Dependencies

- `git`

## 🤹 Example Playbook

*TODO*

## ⚖️ License

This code is released under the [GPLv3](./LICENSE) license. For more information refer to `LICENSE.md`

## ✍️ Author Information

Gerson Rojas <thegrojas@protonmail.com>

## References

- https://github.com/RetroPie/RetroPie-Setup/issues/2571
